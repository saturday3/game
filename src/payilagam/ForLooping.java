package payilagam;

public class ForLooping {

	public static void main(String[] args) {
		ForLooping fl=new ForLooping();
	//	fl.do_while();
	//	fl.continue_statement();
		fl.break_statement();
	}

	private void break_statement() {
		// TODO Auto-generated method stub
		int no=1;
		while(no<=100) 
		{
			if(no%3==0 && no%5==0) {
				System.out.println(no+" ");
				if(no==75) {
				break;
				}
			}
			no++;
		}
		
	}

	private void continue_statement() {
		// TODO Auto-generated method stub
		int no=1;
		while(no<=5) {
			System.out.println(no+" ");
			no++;
			if (no==4) {
				no++;
				continue;
			}
		}
		
	}

	private void do_while() {
		// TODO Auto-generated method stub
		int no=6;
		do {
			System.out.println(no+" ");
			no++;
		}while(no<=5);
					
		
	}

}
